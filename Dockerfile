ARG ruby_version="2.7"

FROM ruby:${ruby_version}

RUN set -eux; \
	gem install bundler-audit 'bundler:~>2.0' --no-doc; \
	useradd -rm bundler-audit

USER bundler-audit

WORKDIR /data

VOLUME /data

ENTRYPOINT [ "bundle-audit" ]

CMD [ "check", "--update" ]
