SHELL = /bin/bash

build_tag ?= bundler-audit
ruby_version ?= 2.7

.PHONY : build
build:
	docker build --pull -t $(build_tag) \
		--build-arg ruby_version=$(ruby_version) \
		- < ./Dockerfile

.PHONY : clean
clean:
	echo 'no-op'

.PHONY : test
test:
	docker run --rm -v "$(shell pwd)/test:/data" $(build_tag)
